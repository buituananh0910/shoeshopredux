import React, { Component } from "react";

export default class TableUser extends Component {
  renderTbody = () => {
    return this.props.userList.map((user) => {
      return (
        <tr>
          <td>{user.id}</td>
          <td>{user.account}</td>
          <td>{user.username}</td>
          <td>{user.email}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => this.props.handleUserDelete(user.id)}
            >
              Xoá
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => this.props.handleUserEdit(user.id)}
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Tài Khoản</th>
              <th>Họ Tên</th>
              <th>Email</th>
              <th>Setting</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

import React, { Component } from "react";
import { initialUserForm } from "./utils";
import { nanoid } from "nanoid";
export default class FormUser extends Component {
  state = {
    user: initialUserForm,
  };
  handleChangeUsername = (event) => {
    // console.log(event);
    // this.setState({
    //   user: { ...this.state.user, account: event.target.value },
    // });
    let { name, value } = event.target;
    this.setState({
      user: { ...this.state.user, [name]: value },
    });
  };
  handleAddUser = () => {
    let newUser = { ...this.state.user };
    newUser.id = nanoid();
    this.props.handleUserAdd(newUser);
  };
  static getDerivedStateFromProps(newProps, state) {
    console.log("props, state: ", newProps, state);
  }
  render() {
    console.log(this.state.user);
    return (
      <div className="container">
        <form>
          <div className="form-group">
            <input
              onChange={(event) => this.handleChangeUsername(event)}
              value={this.state.user.account}
              type="text"
              name="account"
              id
              className="form-control"
              placeholder="Tài Khoản"
              aria-describedby="helpId"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(event) => this.handleChangeUsername(event)}
              value={this.state.user.username}
              type="text"
              name="username"
              id
              className="form-control"
              placeholder="Họ Tên"
              aria-describedby="helpId"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(event) => this.handleChangeUsername(event)}
              value={this.state.user.email}
              type="text"
              name="email"
              id
              className="form-control"
              placeholder="Email"
              aria-describedby="helpId"
            />
          </div>
          <button
            type="button"
            className="btn btn-warning"
            onClick={() => this.handleAddUser()}
          >
            Add User
          </button>
        </form>
      </div>
    );
  }
}

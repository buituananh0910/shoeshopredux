import React, { Component } from "react";
import FormUser from "./FormUser";
import TableUser from "./TableUser";
import { findIndex, _ } from "lodash";
export default class Ex_User_Management extends Component {
  state = {
    userList: [],
    userEdited: "",
  };
  handleUserAdd = (user) => {
    let cloneUserList = [...this.state.userList, user];
    this.setState({
      userList: cloneUserList,
    });
  };
  handleUserDelete = (id) => {
    let index = this.state.userList.findIndex((item) => {
      if (item.id == id) {
        return item;
      }
    });
    console.log("index: ", index);
  };
  handleUserEdit = (id) => {
    let index = this.state.userList.findIndex((item) => {
      if (item.id == id) {
        return item;
      }
    });
    if (index == -1) return;
    let userEdited = this.state.userList[index];
    this.setState({ userEdited: userEdited });
  };
  render() {
    return (
      <div>
        <FormUser handleUserAdd={this.handleUserAdd} />
        <TableUser
          userList={this.state.userList}
          handleUserDelete={this.handleUserDelete}
          userEdited={this.state.userEdited}
        />
      </div>
    );
  }
}

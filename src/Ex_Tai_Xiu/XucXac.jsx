import React, { Component } from "react";
import { connect } from "react-redux";
import { LUA_CHON, TAI, XIU } from "./constant/constant";

let btnStyle = {
  width: 150,
  height: 150,
  fontSize: 44,
};
export class XucXac extends Component {
  render() {
    let { mangXucXac } = this.props;
    return (
      <div className="container pt-5">
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <button
            style={btnStyle}
            className="btn btn-danger"
            onClick={() => this.props.handleSelectOption(TAI)}
          >
            Tài
          </button>
          <div>
            {/* render mảng xúc xắc */}
            {mangXucXac.map((item) => {
              return <img style={{ width: 100 }} src={item.img} alt="" />;
            })}
          </div>
          <button
            style={btnStyle}
            className="btn btn-secondary"
            onClick={() => this.props.handleSelectOption(XIU)}
          >
            Xỉu
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  mangXucXac: state.xucXacReducer.mangXucXac,
});

const mapDispatchToProps = (dispatch) => {
  return {
    handleSelectOption: (luaChon) => {
      dispatch({
        type: LUA_CHON,
        payload: luaChon,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucXac);

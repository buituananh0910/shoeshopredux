import React, { Component } from "react";
import bgGame from "../assets/bgGame.png";
import "./game.css";
import KetQua from "./KetQua";
import XucXac from "./XucXac";
export default class Ex_Tai_Xiu extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bgGame})`,
          width: "100vw",
          height: "100vh",
        }}
      >
        <XucXac />
        <KetQua />
      </div>
    );
  }
}

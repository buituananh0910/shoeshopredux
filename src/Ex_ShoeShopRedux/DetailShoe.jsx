import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let { name, image, price, description } = this.props.detailShoe;

    return (
      <div className="container p-5">
        <div className="row">
          <div className="col-4">
            <img src={image} alt="" className="w-100" />
          </div>
          <div className="col-8">
            <p>Tên: {name}</p>
            <p>Giá: {price}</p>
            <p>Mô tả: {description}</p>
          </div>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detailShoe: state.shoeReducer.detailShoe,
  };
};
// let mapDispathToProps = () => {};
export default connect(mapStateToProps, null)(DetailShoe);

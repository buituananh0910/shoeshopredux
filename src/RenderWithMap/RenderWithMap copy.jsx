import React, { Component } from "react";

export default class RenderWithMap extends Component {
  state = {
    phoneArr: [
      {
        name: "Manx",
        img: "http://loremflickr.com/640/480/technics",
        price: "89.00",
        id: "1",
      },
      {
        name: "Burmese",
        img: "http://loremflickr.com/640/480/technics",
        price: "467.00",
        id: "2",
      },
      {
        name: "Sokoke",
        img: "http://loremflickr.com/640/480/technics",
        price: "478.00",
        id: "3",
      },
      {
        name: "Scottish Fold",
        img: "http://loremflickr.com/640/480/technics",
        price: "245.00",
        id: "4",
      },
      {
        name: "Minskin",
        img: "http://loremflickr.com/640/480/technics",
        price: "584.00",
        id: "5",
      },
      {
        name: "Turkish Angora",
        img: "http://loremflickr.com/640/480/technics",
        price: "534.00",
        id: "6",
      },
      {
        name: "Serengeti",
        img: "http://loremflickr.com/640/480/technics",
        price: "617.00",
        id: "7",
      },
      {
        name: "Selkirk Rex",
        img: "http://loremflickr.com/640/480/technics",
        price: "487.00",
        id: "8",
      },
      {
        name: "Pixiebob",
        img: "http://loremflickr.com/640/480/technics",
        price: "439.00",
        id: "9",
      },
      {
        name: "Birman",
        img: "http://loremflickr.com/640/480/technics",
        price: "9.00",
        id: "10",
      },
      {
        name: "Bengal",
        img: "http://loremflickr.com/640/480/technics",
        price: "609.00",
        id: "11",
      },
    ],
  };
  render() {
    return (
      <div>
        <p>RenderWithMap</p>
        <div className="row">
          {this.state.phoneArr.map((item, index) => {
            return (
              <div
                key={index.toString() + item.id}
                className="card col-2"
                style={{ width: "18rem" }}
              >
                <img
                  className="card-img-top"
                  src={item.img}
                  alt="Card image cap"
                />
                <div className="card-body">
                  <h5 className="card-title">{item.name}</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    {item.price}
                  </a>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

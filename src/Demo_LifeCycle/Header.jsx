import React, { Component, PureComponent } from "react";

export default class Header extends PureComponent {
  shouldComponentUpdate(nextProps, nextState) {
    console.log("nextProps: ", nextProps);
  }
  render() {
    console.log("header render");
    return (
      <div className="text-center bg-warning">
        <p>Header</p>
        <p>Like : {this.props.like}</p>
      </div>
    );
  }
  componentWillUnmount() {
    console.log("header die");
  }
}

// pure component ~ Hạn chế render không cần thiết ~ pass by value

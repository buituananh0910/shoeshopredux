import React, { Component, PureComponent } from "react";
import Header from "./Header";
console.log("didmount");
export default class extends Component {
  state = {
    like: 0,
    share: 0,
  };
  render() {
    console.log("rener");
    return (
      <div className="text-center">
        <Header like={this.state.like} share={this.state.share} />
        <div>
          <span className="display-4">{this.state.like}</span>
          <button
            className="btn btn-success"
            onClick={() => this.setState({ like: this.state.like + 1 })}
          >
            Plus Like
          </button>
          <span className="display-4">{this.state.share}</span>
          <button
            className="btn btn-success"
            onClick={() => this.setState({ share: this.state.share + 1 })}
          >
            Plus Share
          </button>
        </div>
      </div>
    );
  }
}

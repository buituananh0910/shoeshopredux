import React, { Component } from "react";
import { connect } from "react-redux";
class DemoReduxMini extends Component {
  render() {
    return (
      <div className="text-center py-5">
        <button
          className="btn btn-secondary"
          onClick={() => this.props.giamSoLuong(10)}
        >
          -
        </button>
        <span className="display-4 text-secondary">{this.props.number}</span>
        <button
          className="btn btn-warning"
          onClick={() => this.props.tangSoLuong()}
        >
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.number.soLuong,
    isLogin: state.number.isLogin,
  };
};

let mapDispathToProps = (dispath) => {
  return {
    tangSoLuong: () => {
      let action = {
        type: "TangSoLuong",
      };
      dispath(action);
    },
    giamSoLuong: (value) => {
      let action = {
        type: "GiamSoLuong",
        payload: value,
      };
      dispath(action);
    },
  };
};
export default connect(mapStateToProps, mapDispathToProps)(DemoReduxMini);

import { numberReducer } from "./numberReducer";
import { combineReducers } from "redux";

export let rootReducer_DemoReduxMini = combineReducers({
  number: numberReducer,
});

let initalState = {
  soLuong: 100,
  isLogin: true,
};

export let numberReducer = (state = initalState, action) => {
  switch (action.type) {
    case "TangSoLuong": {
      state.soLuong++;
      return { ...state };
    }
    case "GiamSoLuong": {
      state.soLuong -= action.payload;
      return { ...state };
    }
    default: {
      return state;
    }
  }
};
